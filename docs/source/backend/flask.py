#!/usr/bin/env python3

from jsonrpc import JSONRPC
from jsonrpc.backend.flask import APIView
from flask import Flask

app = Flask(__name__)

jsonrpc = JSONRPC()
app.add_url_rule('/api', view_func=APIView.as_view('api', jsonrpc=jsonrpc))


@jsonrpc.method('boo')
def index() -> str:
    return 'Welcome to JSON-RPC'


def link_page_tag(tag: int, page: int) -> str:
    return f'tag: {tag}\npage: {page}'


jsonrpc['tag.page'] = link_page_tag
jsonrpc['page.tag'] = link_page_tag


def raise_error() -> bool:
    raise ValueError("raise ValueError")
    return True


jsonrpc['raise.error'] = raise_error

app.run(host='0.0.0.0', debug=True)
