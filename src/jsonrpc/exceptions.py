__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'


class JSONRPCError(Exception):
    def __init__(self, id: int, message):
        pass

    def __json__(self):
        result = {
            "jsonrpc": "2.0",
            "id": self.id,
            "error": {
                "code": self.CODE,
                "message": self.message
            }
        }
        return result


class InvalidRequestError(JSONRPCError):
    CODE = -32600

    def __init__(self, id: int, message: str = 'Invalid Request'):
        self.id = id
        self.message = message
        self.code = CODE


class ParseError(JSONRPCError):
    CODE = -32700

    def __init__(self, id: int, message: str = 'Parse error'):
        self.id = id
        self.message = message
        self.code = CODE


class MethodNotFoundError(JSONRPCError):
    CODE = -32601

    def __init__(self, id: int, message: str = 'Method not found'):
        self.id = id
        self.message = message
        self.code = self.CODE


class InvalidParamsError(JSONRPCError):
    CODE = -32602

    def __init__(self, id: int, message: str = 'Invalid params'):
        self.id = id
        self.message = message
        self.code = self.CODE


class InternalError(JSONRPCError):
    """Internal JSON-RPC error
    """
    CODE = -32603

    def __init__(self, id: int, message: str = 'Internal error'):
        self.id = id
        self.message = message
        self.code = self.CODE


class ServerError(JSONRPCError):
    """Reserved for implementation-defined server-errors.

    code: -32000 to -32099 Server error.
    """
    CODE = -32000

    def __init__(self, id: int, message: str = 'Server error'):
        self.id = id
        self.message = message
        self.code = self.CODE
