Установка
=========

Сборка
------

.. code-block:: bash

   python -m build

Установка
---------


.. code-block:: bash

   pip install -e .
