Использование
=============

.. code-block:: python

   from jsonrpc import JSONRPC


   jsonrpc = JSONRPC()

   @jsonrpc.method('app.endpoint')
   def app_endpoint(a: int, b: int) -> int:
       result = a + b
       return result

   request = {
       "jsonrpc": "2.0",
       "method": "app.endpoint",
       "params": {"a": 1, "b": 2},
       "id": "1"
   }
   response = jsonrpc(request)

   print(response)
