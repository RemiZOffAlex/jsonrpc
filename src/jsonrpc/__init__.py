__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import json
import logging
import traceback

from inspect import signature
from .exceptions import *


log = logging.getLogger(__name__)


class Response:
    def __init__(self, id: int, result):
        self.id = id
        self.result = result

    def __json__(self):
        response = {
            "jsonrpc": "2.0",
            "result": self.result,
            "id": self.id,
        }
        return response


class Method:
    def __init__(self, function, pre = None, debug: bool = False):
        self.function = function
        self.pre = pre
        self.debug = debug

    def __call__(self, query):
        params = None
        if 'params' in query:
            params = query['params']
            if self.debug:
                log.error(params)
        if isinstance(self.pre, list):
            for item in self.pre:
                item(query)
        elif callable(self.pre):
            self.pre(query)


        if params is None:
            response = self.function()
        elif isinstance(params, list):
            response = self.function(*params)
        elif isinstance(params, dict):
            response = self.function(**params)
        else:
            raise InvalidParamsError(
                id=query['id'],
                message='Invalid params: {0}'.format(params)
            )

        return response


    def __repr__(self):
        return '<{}>'.format(self.function)


class Wrapper:
    def __init__(self, func):
        self.func = func


class JSONRPC:
    """Основной класс JSON-RPC
    """
    def __init__(self, debug: bool = False):
        self.methods = {}
        self.debug = debug

    def method(self, name: str, pre = None):
        """Декоратор метода
        """
        assert len(name) > 0, 'Не указано имя метода'

        def wrap(function):
            method = Method(
                function = function,
                pre = pre
            )
            self.methods[name] = method
            return function
        return wrap

    def description(self, name: str):
        """Описание процедуры
        """
        if name not in self.methods:
            return None
        method = self.methods[name]
        sig = signature(method.function)
        # for key in sig.parameters:
        #     print(sig.parameters[key].annotation)
        result = {
            'name': name,
            'function': getattr(method.function, '__name__', None),
            'summary': getattr(method.function, '__doc__', None),
            'params': [
                {'name': k, 'type': sig.parameters[k].annotation.__name__}
                for k in sig.parameters
            ],
            'return': sig.return_annotation.__name__,
        }
        return result

    def example(self, name: str):
        """Пример
        """
        if name not in self.methods:
            return None
        method = self.methods[name]
        sig = signature(method.function)

        if len(sig.parameters) == 0:
            result = {
                "jsonrpc": "2.0",
                "method": name,
                "id": 1
            }
        else:
            params = {}
            for key in sig.parameters:
                params[key] = ''
            result = {
                "jsonrpc": "2.0",
                "method": name,
                "params": params,
                "id": 1
            }

        return result

    def validate(self, query):
        """Валидация запроса
        """
        keys = query.keys()
        if 'id' not in query:
            return InvalidRequestError(
                id=query['id'],
                message=f'Некорректный запрос: {query}'
            ).__json__()

        if 'method' not in query:
            return InvalidRequestError(
                id=query['id'],
                message=f'Некорректный запрос: {query}'
            ).__json__()

    def process(self, query):
        """Выполнение метода
        """
        self.validate(query)
        name = query['method']
        if name not in self.methods:
            result = MethodNotFoundError(
                query['id'],
                message=f'Метод не найден: {name}'
            )
            return result.__json__()


        method = self.methods[name]

        try:
            response = method(query)
        except JSONRPCError as e:
            log.error(traceback.format_exc())
            # print(traceback.format_exc())
            # response = traceback.format_exc()
            response = InternalError(
                id=query['id'],
                message=str(e)
                # message=traceback.format_exc()
            )
        except Exception as e:
            log.error(traceback.format_exc())
            response = InternalError(
                id=query['id'],
                message=str(e)
                # message=traceback.format_exc()
            )
        else:
            response = Response(
                id=query['id'],
                result=response
            )
        result = response.__json__()
        return result

    def __call__(self, queries):
        """Вызов метода
        """
        if isinstance(queries, dict):
            result = self.process(queries)
        elif isinstance(queries, list):
            result = []
            for query in queries:
                result.append(self.process(query))
        return result

    def __getitem__(self, key):
        method = self.methods[key]
        return method.function

    def __iter__(self):
        return iter(self.methods)

    def __len__(self):
        return len(self.methods)

    def __setitem__(self, key, function, pre=None):
        method = Method(function=function, pre=pre)
        self.methods[key] = method

    def __delitem__(self, key):
        del self.methods[key]

    def __repr__(self):
        return repr(self.methods)
