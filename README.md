# JSON-RPC

## Установка

```
pip install -e .
```

## Документация

```
pip install sphinx
make -C docs html
```

Открыть в браузере docs/build/html/index.html

## Ссылки

* [http://docs.python.org/](http://docs.python.org/)
* [http://www.jsonrpc.org/](http://www.jsonrpc.org/)
