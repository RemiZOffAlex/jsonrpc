.. _index:

.. JSON-RPC documentation master file, created by
   sphinx-quickstart on Fri Feb 18 23:17:10 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация JSON-RPC!
======================

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   description
   install
   usage
   backend/index
   specification

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
