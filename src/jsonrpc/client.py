__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import json
import logging
import requests


log = logging.getLogger(__name__)


class Client:
    def __init__(
        self,
        url: str = 'http://127.0.0.1:5000/api',
        debug: bool = False
    ):
        self.url = url
        self.headers = {'content-type': 'application/json'}
        self.debug = debug

    def __call__(self, queries):
        """Вызов метода
        """
        if isinstance(queries, str):
            payload = queries
        elif isinstance(queries, dict | list):
            payload = json.dumps(queries)
        response = requests.post(
            self.url,
            data=payload,
            headers=self.headers
        ).json()

        assert 'jsonrpc' in response
        assert 'id' in response
        assert response["jsonrpc"] in ['2.0', '3.0']
        if '3.0' in response["jsonrpc"]:
            assert 'meta' in response

        return response
